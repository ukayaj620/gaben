@extends('base')

@section('main')
<nav class="self-start flex mt-4 ml-3" aria-label="Breadcrumb">
  <ol class="inline-flex items-center space-x-1 md:space-x-3">
    <li class="inline-flex items-center">
      <a href="{{ url('/') }}" class="inline-flex items-center text-sm font-medium text-gray-400 hover:text-gray-700 dark:text-gray-400 dark:hover:text-white">
        <svg class="mr-2 w-4 h-4" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"></path></svg>
        Home
      </a>
    </li>
    <li>
      <div class="flex items-center">
        <svg class="w-6 h-6 text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path></svg>
        <span class="ml-1 text-sm font-medium text-gray-400 md:ml-2">{{ $game->category->first()->name }}</a>
      </div>
    </li>
    <li aria-current="page">
      <div class="flex items-center">
        <svg class="w-6 h-6 text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path></svg>
        <span class="ml-1 text-sm font-medium text-gray-400 md:ml-2 dark:text-gray-500">{{ $game->name }}</span>
      </div>
    </li>
  </ol>
</nav>
<div class="flex w-full justify-between pt-4">
  <div class="w-3/5">
    <video controls class="w-full rounded-lg">
      <source src="{{ $game->trailer_path }}">
    </video>
  </div>
  <div class="flex flex-col h-full w-1/3 space-y-4 text-gray-700">
    <img src="{{ $game->cover_path }}" class="rounded-lg w-full shadow-md"/>
    <h4 class="text-2xl font-bold text-gray-700">{{ $game->name }}</h4>
    <p>{{ $game->description }}</p>
    <div class="flex flex-col space-y-1">
      <p><span class="font-semibold">Genre: </span>{{ $game->category->first()->name }}</p>
      <p><span class="font-semibold">Release Date: </span>{{ date('d-m-Y') }}</p>
      <p><span class="font-semibold">Developer: </span>{{ $game->developer }}</p>
      <p><span class="font-semibold">Publisher: </span>{{ $game->publisher }}</p>
    </div>
  </div>
</div>
<div class="relative w-full bg-gray-50 px-4 py-6 rounded-lg shadow-lg mt-8 mb-12">
  <p class="font-bold text-gray-700">Buy {{ $game->name }}</p>
  <form class="bg-slate-600 py-2.5 px-2 rounded-md text-gray-50 absolute -bottom-5 right-4" method="POST" action="{{ url('/cart/add') }}">
    @csrf
    <input type="hidden" name="gameId" value="{{ $game->id }}">
    <button type="submit" class="flex justify-between rounded-md space-x-2">
      <p class="font-semibold">Rp. {{ $game->price }}</p>
      <p class="font-semibold">|</p>
      <div class="flex space-x-2 items-center">
        <img class="w-6 h-6 rounded-full" src="/images/cart.svg" alt="cart logo">
        <p class="font-semibold text-sm">ADD TO CART</p>
      </div>
    </button>
  </form>
</div>
<div class="w-full flex flex-col text-gray-700">
  <h4 class="text-2xl font-semibold">ABOUT THIS GAME</h4>
  <div class="bg-gray-700 w-full h-1 mt-1 mb-4"></div>
  <p class="w-full">{{ $game->long_description }}</p>
</div>
@endsection
