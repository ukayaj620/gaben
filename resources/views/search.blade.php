@extends('base')

@section('main')
  <h2 class="text-2xl font-bold text-gray-700 my-4 ml-3 self-start">Search Games</h2>
  <div class="flex flex-wrap">
    @if (count($games) == 0)
      <p class="text-gray-500">There are no games content can be showed right now.</p>
    @else
      @foreach ($games as $game)
      <a href="{{ url('/', ['gameId' => $game->id]) }}" class="relative rounded-lg shadow-lg flex flex-shrink-0 cursor-pointer" style="margin: 24px 12px">
        <img src="{{ $game->cover_path }}" class="rounded-lg" width="360"/>
        <div class="absolute bg-white px-3 py-2 rounded-md z-20" style="left: 8px; bottom: 8px; opacity: 0.88">
          <h6 class="font-semibold">{{ $game->name }}</h6>
          <p class="text-sm">{{ $game->category()->first()->name }}</p>
        </div>
        <div class="absolute inset-0 bg-white w-full h-full" style="opacity: 0.36"></div>
      </a>
      @endforeach
    @endif
  </div>
@endsection
