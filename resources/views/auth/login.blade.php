@extends('auth.base')

@section('form')
<div class="w-96 flex flex-col">
  <h2 class="text-3xl font-black text-gray-100 mb-12">Login Page</h2>
  <form class="w-full" method="post" action="{{ url('/login') }}">
    @csrf
    <div class="mb-6">
      <label for="username" class="block mb-2 font-semibold text-gray-100">Username</label>
      <input type="username" id="username" name="username" class="bg-gray-50 border border-gray-300 text-gray-900 rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2" required>
    </div>
    <div class="mb-6">
      <label for="password" class="block mb-2 font-semibold text-gray-100">Password</label>
      <input type="password" id="password" name="password" class="bg-gray-50 border border-gray-300 text-gray-900 rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2" required="">
    </div>
    <div class="flex items-start mb-6">
      <div class="flex items-center h-5">
        <input id="remember" name="remember" aria-describedby="remember" type="checkbox" class="w-4 h-4 bg-gray-50 rounded border border-gray-300 focus:ring-3 focus:ring-blue-300">
      </div>
      <div class="ml-3">
        <label for="remember" class="font-semibold text-gray-100">Remember me</label>
      </div>
    </div>
    <button type="submit" class="text-white bg-indigo-700 hover:bg-indigo-800 focus:ring-4 focus:ring-indigo-300 rounded-lg  w-full px-5 py-2 text-center font-semibold">Sign In</button>
  </form>
  <a class="text-indigo-700 no-underline font-semibold self-end mt-6" href="{{ url('/register') }}">Don't have account</a>
</div>

@endsection