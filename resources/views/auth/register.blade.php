@extends('auth.base')

@section('form')
<div class="w-96 flex flex-col">
  <h2 class="text-3xl font-black text-gray-100 mb-12">Register Page</h2>
  <form class="w-full" method="post" action="{{ url('/register') }}">
    @csrf
    <div class="mb-6">
      <label for="username" class="block mb-2 font-semibold text-gray-100">Username</label>
      <input name="username" type="username" id="username" value="{{ old('username') }}" class="bg-gray-50 border border-gray-300 text-gray-900 rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2" required>
    </div>
    <div class="mb-6">
      <label for="fullname" class="block mb-2 font-semibold text-gray-100">Full Name</label>
      <input name="fullname" type="text" id="fullname" value="{{ old('fullname') }}" class="bg-gray-50 border border-gray-300 text-gray-900 rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2" required>
    </div>
    <div class="mb-6">
      <label for="password" class="block mb-2 font-semibold text-gray-100">Password</label>
      <input name="password" type="password" id="password" value="{{ old('password') }}" class="bg-gray-50 border border-gray-300 text-gray-900 rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2" required>
    </div>
    <div class="mb-6">
      <label for="role" class="block mb-2 font-semibold text-gray-100">Role</label>
      <select name="role" id="role" class="bg-gray-50 border border-gray-300 text-gray-800 rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2">
        <option>Member</option>
        <option>Admin</option>
      </select>
    </div>
    <button type="submit" class="text-white bg-indigo-700 hover:bg-indigo-800 focus:ring-4 focus:ring-indigo-300 rounded-lg  w-full px-5 py-2 text-center font-semibold">Sign Up</button>
  </form>
  <a class="text-indigo-700 no-underline font-semibold self-end mt-6" href="{{ url('/login') }}">Already have account?</a>
</div>

@endsection