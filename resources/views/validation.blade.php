@extends('base')

@section('main')
<div class="w-full flex flex-col items-center">
  <div class="relative w-4/5 border-4 border-gray-700 rounded-lg flex flex-col justify-center items-center mt-16 py-8">
    <div class="h-16"></div>
    <img src="{{ $game->cover_path }}" width="240" class="rounded-lg absolute -top-12 left-2/5"/>
    <form class="w-full flex flex-col justify-center items-center" method="POST" action="{{ url('/validation', ['gameId' => $game->id ]) }}">
      @csrf
      <p class="w-1/2 text-center font-semibold text-gray-700">CONTENT IN THIS PRODUCT MAY NOT BE APPROPRIATE FOR ALL AGES, OR MAY NOT BE APPROPRIATE FOR VIEWING AT WORK</p>
      <div class="w-1/2 py-8 flex flex-col justify-center items-center bg-gray-100 rounded-md mt-4 shadow-lg">
        <p class="w-3/5 text-center text-gray-700">Please enter your birth date to continue:</p>
        <div class="relative mt-4">
          <div class="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
            <svg class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd"></path></svg>
          </div>
          <input datepicker type="text" name="birth-date" class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Select date">
        </div>
      </div>
      <div class="flex space-x-4 mt-4">
        <input type="submit" value="View Page" class="text-white bg-gray-700 hover:bg-gray-800 focus:ring-4 focus:ring-gray-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">
        <button type="button" class="text-gray-900 bg-white border border-gray-300 hover:bg-gray-100 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">Cancel</button>
      </div>
    </form>
  </div>
</div>
@endsection