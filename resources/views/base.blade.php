<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://unpkg.com/@themesberg/flowbite@1.3.0/dist/flowbite.min.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body class="w-screen flex flex-col items-center bg-gray-200 overflow-x-hidden">
  <nav class="w-full border-gray-200 px-8 py-2.5 bg-gray-800">
    <div class="flex flex-wrap justify-between items-center mx-auto">
      <div class="flex items-center">
        <a href="{{ url('/') }}" class="flex">
          <p class="text-3xl text-gray-100 font-bold">ReXsteam</p>
        </a>
        <ul class="flex items-center ml-8 text-sm">
          <li>
            <a href="{{ url('/') }}" class="block py-2 pr-4 pl-3 border-b md:border-0 md:p-0 md:hover:text-white text-gray-400 hover:bg-gray-700 hover:text-white md:hover:bg-transparent border-gray-700">Home</a>
          </li>
          @auth
          @if (auth()->user()->role->first()->name == 'Admin')
          <li>
            <a href="#" class="ml-8 block py-2 pr-4 pl-3 border-b md:border-0 md:p-0 md:hover:text-white text-gray-400 hover:bg-gray-700 hover:text-white md:hover:bg-transparent border-gray-700">Manage Games</a>
          </li>          
          @endif
          @endauth
        </ul>
      </div>
      <div class="flex items-center">
        <div class="hidden relative mr-3 md:mr-0 md:block">
          <div class="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
            <svg class="w-5 h-5 text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path></svg>
          </div>
          <form action="{{ url('/search') }}" method="GET">
            <input type="text" name="query" id="email-adress-icon" class="block p-2 pl-10 w-full bg-gray-700 border-gray-600 placeholder-gray-400 rounded-lg text-white focus:ring-blue-500 focus:border-blue-500" value="{{ old('query') }}" placeholder="Search...">
          </form>
        </div>
        @guest
        <ul class="flex ml-8 items-center">
          <li class="mr-4">
            <a href="{{ url('/login') }}" class="block py-2 pr-4 pl-3 border-b text-sm md:border-0 md:p-0 md:hover:text-white text-gray-400 hover:bg-gray-700 hover:text-white md:hover:bg-transparent border-gray-700">Login</a>
          </li>
          <li>
            <a href="{{ url('/register') }}" class="block py-2 pr-4 pl-3 text-sm border-b md:border-0 md:p-0 text-gray-400 md:hover:text-white hover:bg-gray-700 hover:text-white md:hover:bg-transparent border-gray-700">Register</a>
          </li>
        </ul>
        @endguest
        @auth
        @if (auth()->user()->role->first()->name == 'Member')
        <a href="{{ url('/cart') }}" class="ml-8 flex text-sm bg-gray-800 rounded-full focus:ring-4 focus:ring-gray-300">
          <img class="w-6 h-6 rounded-full" src="/images/cart.svg" alt="cart logo">
        </a>
        @endif
        <button type="button" class="ml-8 flex text-sm bg-gray-800 rounded-full focus:ring-4 focus:ring-gray-300" id="user-menu-button" aria-expanded="false" type="button" data-dropdown-toggle="dropdown">
          <span class="sr-only">Open user menu</span>
          <img class="w-8 h-8 rounded-full" src="https://upload.wikimedia.org/wikipedia/commons/b/b1/LinuxCon_Europe_Linus_Torvalds_05.jpg" alt="user photo">
        </button>
        <div class="hidden w-48 z-50 my-4 text-base list-none bg-white rounded divide-y divide-gray-100 shadow" id="dropdown">
          <ul class="py-1" aria-labelledby="dropdown">
            <li>
              <a href="#" class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100">Profile</a>
            </li>
            @if (auth()->user()->role->first()->name == 'Member')
            <li>
              <a href="#" class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100">Friends</a>
            </li>
            <li>
              <a href="#" class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100">Transaction History</a>
            </li>   
            @endif
            <li>
              <a href="{{ url('/logout') }}" class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100">Sign out</a>
            </li>
          </ul>
        </div>
        @endauth
      </div>
    </div>
  </nav>
  <main class="w-full flex flex-col px-8 items-center">
    @if (session('alert'))
    @php
        $alertColor = session('alert')['color']
    @endphp
    <div id="alert-2" class="flex p-4 mb-4 w-1/2 bg-{{ $alertColor }}-100 rounded-lg absolute z-10 mt-4" role="alert">
      <svg class="flex-shrink-0 w-5 h-5 text-{{ $alertColor }}-700" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
      <div class="ml-3 text-sm font-medium text-{{ $alertColor }}-700">
        <div>{{ session('alert')['message'] }}</div>
      </div>
      <button type="button" class="ml-auto -mx-1.5 -my-1.5 bg-{{ $alertColor }}-100 text-{{ $alertColor }}-500 rounded-lg focus:ring-2 focus:ring-{{ $alertColor }}-400 p-1.5 hover:bg-{{ $alertColor }}-200 inline-flex h-8 w-8" data-collapse-toggle="alert-2" aria-label="Close">
        <span class="sr-only">Close</span>
        <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
      </button>
    </div>
    @endif
    @yield('main')
  </main>
  <footer class="w-screen bg-gray-800 flex justify-between p-8 mt-8">
    <p class="text-gray-400">© 2022 ReXsteam. All rights reserved.</p>
  </footer>
  <script src="https://unpkg.com/@themesberg/flowbite@1.3.0/dist/flowbite.bundle.js"></script>
  <script src="https://unpkg.com/@themesberg/flowbite@1.3.0/dist/datepicker.bundle.js"></script>
  <script src="https://unpkg.com/ionicons@5.5.2/dist/ionicons.js"></script>
  @yield('scripts')
</body>
</html>