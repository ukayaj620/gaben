@extends('base')

@section('main')
<h2 class="text-2xl font-bold text-gray-700 my-4 self-start">Shopping Cart</h2>
@if (count($games) == 0)
  <p class="text-gray-500">There are no games content can be showed right now.</p>
@else
<div class="flex flex-col p-2 w-full bg-gray-50 rounded-lg shadow-lg">
  @php
    $totalPrice = 0
  @endphp
  @foreach ($games as $game)
  @php
    $totalPrice += $game->price
  @endphp
  <div class="flex flex-col w-full space-y-2 pb-4">
    <div class="flex w-full items-center mt-2 p-2">
      <img src="{{ $game->cover_path }}" class="rounded-lg" width="180"/>
      <div class="flex flex-col space-y-2 ml-8">
        <div class="flex space-x-2 items-center">
          <h6 class="text-lg text-gray-800 font-semibold">{{ $game->name }}</h6>
          <div class="px-2 py-1 rounded-full bg-gray-700 flex items-center">
            <p class="text-xs m-0 text-gray-50">{{ $game->category->first()->name }}</p>
          </div>
        </div>
        <div class="flex space-x-2 items-center">
          <ion-icon name="pricetag-outline" class="text-gray-500"></ion-icon>
          <p class="text-sm m-0 text-gray-500">Rp. {{ $game->price }},-</p>
        </div>
      </div>
      <button type="button" data-modal-toggle="delete-confirmation-popup-{{ $game->id }}" class="ml-auto justify-self-end text-white space-x-2 bg-gray-300 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center mr-2">
        <ion-icon name="trash-outline" class="text-gray-800 font-semibold"></ion-icon>
        <p class="text-sm m-0 text-gray-800">Delete</p>
      </button>
    </div>
    <div class="bg-gray-200 w-full" style="height: 0.125rem"></div>
    <div class="hidden overflow-y-auto overflow-x-hidden fixed right-0 left-0 top-4 z-50 justify-center items-center md:inset-0 h-modal sm:h-full" id="delete-confirmation-popup-{{ $game->id }}">
      <div class="relative px-4 w-full max-w-md h-full md:h-auto">
        <div class="relative bg-white rounded-lg shadow">
          <div class="flex justify-end p-2">
            <button type="button" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center" data-modal-toggle="delete-confirmation-popup-{{ $game->id }}">
              <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>  
            </button>
          </div>
          <div class="p-6 pt-0 text-center">
            <svg class="mx-auto mb-4 w-14 h-14 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
            <h3 class="mb-5 text-lg font-normal text-gray-500">Are you sure you want to delete this product?</h3>
            <button id="remove-item-{{ $game->id }}" data-modal-toggle="delete-confirmation-popup-{{ $game->id }}" type="button" class="remove-item-button text-white bg-red-600 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm inline-flex items-center px-5 py-2.5 text-center mr-2">
              Delete
            </button>
            <button data-modal-toggle="delete-confirmation-popup-{{ $game->id }}" type="button" class="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:ring-gray-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10">Cancel</button>
          </div>
        </div>
      </div>
  </div>
  </div>
  @endforeach
  <div class="text-gray-800 ml-2">
    Total Price: <span class="font-semibold">Rp. {{ $totalPrice }},-</span>
  </div>
  <div class="mt-4">
    <a href="{{ url('/transaction') }}" class="mr-auto text-white space-x-2 bg-gray-300 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center ml-2">
      <ion-icon name="bag-check-outline" class="text-gray-800 font-semibold"></ion-icon>
      <p class="text-sm m-0 text-gray-800">Checkout</p>
    </a>
  </div>
</div>
@endif 
@endsection
@section('scripts')
  <script type="text/javascript">
    $(".remove-item-button").click((e) => {
      e.preventDefault();
      const gameId = e.target.id.split('-')[2]
      $.ajax({
        url: '{{ url('/cart/remove') }}',
        method: "DELETE",
        data: {_token: '{{ csrf_token() }}', id: gameId},
        success: (response) => {
          window.location.reload();
        }
      });
    });
  </script>
@endsection