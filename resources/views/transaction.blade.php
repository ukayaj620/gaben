@extends('base')

@section('main')
@if ($errors->any())
<div id="alert-2" class="flex p-4 mb-4 w-1/2 bg-red-100 rounded-lg absolute z-10 mt-4" role="alert">
  <svg class="flex-shrink-0 w-5 h-5 text-red-700" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path>
  </svg>
  <div class="ml-3 text-sm font-medium text-red-700">
    <p class="font-semibold">There were {{ count($errors->all()) }} errors with your submission.</p>
    <ul class="list-disc">
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  <button type="button" class="ml-auto -mx-1.5 -my-1.5 bg-red-100 text-red-500 rounded-lg focus:ring-2 focus:ring-red-400 p-1.5 hover:bg-red-200 inline-flex h-8 w-8" data-collapse-toggle="alert-2" aria-label="Close">
    <span class="sr-only">Close</span>
    <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
    </svg>
  </button>
</div>
@endif
<h2 class="text-2xl font-bold text-gray-700 my-4 self-start">Transaction Information</h2>
<form class="mt-4 w-full flex flex-col" method="POST" action="{{ url('/transaction/checkout') }}">
  @csrf
  <div class="mb-6">
    <label for="card-name" class="block mb-2 text-sm font-medium text-gray-800">Card Name</label>
    <input type="text" name="card-name" id="card-name" class="bg-gray-50 border border-gray-300 text-gray-800 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="Card Name" value="{{ old('card-name') }}" required>
  </div>
  <div class="mb-6">
    <label for="card-number" class="block mb-2 text-sm font-medium text-gray-800">Card Number</label>
    <input type="text" name="card-number" id="card-number" class="bg-gray-50 border border-gray-300 text-gray-800 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="0000 0000 0000 0000" value="{{ old('card-number') }}" required>
  </div>
  <div class="flex justify-between">
    <div class="mb-6 w-96">
      <label for="expire-date-month" class="block mb-2 text-sm font-medium text-gray-800">Expire Date</label>
      <input type="number" name="expire-date-month" id="expire-date-month" class="bg-gray-50 border border-gray-300 text-gray-800 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="MM" value="{{ old('expire-date-month') }}" required>
    </div>
    <div class="mb-6 w-96">
      <label for="expire-date-year" class="block mb-2 text-sm font-medium text-gray-800 h-5"></label>
      <input type="number" name="expire-date-year" id="expire-date-year" class="bg-gray-50 border border-gray-300 text-gray-800 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="YYYY" value="{{ old('expire-date-year') }}" required>
    </div>
    <div class="mb-6 w-96">
      <label for="cvv" class="block mb-2 text-sm font-medium text-gray-800">CVC / CVV</label>
      <input type="text" name="cvv" id="cvv" class="bg-gray-50 border border-gray-300 text-gray-800 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="3 or 4 digits number" value="{{ old('cvv') }}" required>
    </div>
  </div>
  <div class="flex justify-between">
    <div class="mb-6" style="width: 60rem">
      <label for="country" class="block mb-2 text-sm font-medium text-gray-800">Country</label>
      <select name="country" id="country" class="bg-gray-50 border border-gray-300 text-gray-800 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2">
        <option>Indonesia</option>
        <option>Malaysia</option>
      </select>
    </div>
    <div class="mb-6 w-96">
      <label for="zip" class="block mb-2 text-sm font-medium text-gray-800">ZIP</label>
      <input type="text" name="zip" id="zip" class="bg-gray-50 border border-gray-300 text-gray-800 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="ZIP"  value="{{ old('zip') }}" required>
    </div>
  </div>
  <div class="flex justify-between items-center text-gray-800">
    <input type="hidden" name="total-price" value="{{ $totalPrice }}">
    <p>Total Price: <span class="font-semibold">Rp. {{ $totalPrice }},-</span></p>
    <div class="flex space-x-4">
      <a href="{{ url('/cart') }}" class="text-gray-800 bg-white hover:bg-gray-100 focus:ring-4 focus:ring-gray-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10">Cancel</a>
      <button type="submit" class="mr-auto text-white space-x-2 bg-gray-300 focus:ring-4 hover:bg-gray-100 focus:ring-gray-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center ml-2">
        <ion-icon name="bag-check-outline" class="text-gray-800 font-semibold"></ion-icon>
        <p class="text-sm m-0 text-gray-800">Checkout</p>
      </button>
    </div>
  </div>
</form>
@endsection