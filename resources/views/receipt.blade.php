@extends('base')

@section('main')
<h2 class="text-2xl font-bold text-gray-700 my-4 self-start">Transaction Receipt</h2>
<div class="flex flex-col p-2 w-full bg-gray-50 rounded-lg shadow-lg">
  <div class="text-gray-800 font-medium text-sm ml-2 mb-4">
    <h6>Transaction ID: {{ $transaction->transaction_id }}</h6>
    <h6>Purchased Date: {{ $transaction->purchase_date }}</h6>
  </div>
  <div class="bg-gray-200 w-full" style="height: 0.125rem"></div>
  @foreach ($transaction->games as $game)
  <div class="flex flex-col w-full space-y-2 pb-4">
    <div class="flex w-full items-center mt-2 p-2">
      <img src="{{ $game->cover_path }}" class="rounded-lg" width="180"/>
      <div class="flex flex-col space-y-2 ml-8">
        <div class="flex space-x-2 items-center">
          <h6 class="text-lg text-gray-800 font-semibold">{{ $game->name }}</h6>
          <div class="px-2 py-1 rounded-full bg-gray-700 flex items-center">
            <p class="text-xs m-0 text-gray-50">{{ $game->category->first()->name }}</p>
          </div>
        </div>
        <div class="flex space-x-2 items-center">
          <ion-icon name="pricetag-outline" class="text-gray-500"></ion-icon>
          <p class="text-sm m-0 text-gray-500">Rp. {{ $game->price }},-</p>
        </div>
      </div>
    </div>
    <div class="bg-gray-200 w-full" style="height: 0.125rem"></div>
  </div>
  @endforeach
  <div class="text-gray-800 ml-2">
    Total Price: <span class="font-semibold">Rp. {{ $transaction->payment_total }},-</span>
  </div>
</div>
@endsection
