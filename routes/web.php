<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'index', 'uses' => 'HomeController@index']);

Route::get('/search', ['as' => 'search', 'uses' => 'HomeController@search']);

Route::get('/login', ['as' => 'login', function () {
    return view('auth.login');
}])->middleware('guest');

Route::post('/login', ['as' => 'login', 'uses' => 'AuthController@login']);

Route::get('/logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);

Route::get('/register', ['as' => 'register', function () {
    return view('auth.register');
}])->middleware('guest');

Route::post('/register', ['as' => 'register', 'uses' => 'AuthController@register']);

Route::prefix('cart')->middleware('auth')->group(function () {
    Route::delete('/remove', ['as' => 'remove_item', 'uses' => 'CartController@remove_item']);
    Route::post('/add', ['as' => 'add_item', 'uses' => 'CartController@add_item']);
    Route::get('/', ['as' => 'fetch_items', 'uses' => 'CartController@fetch_items']);
});

Route::prefix('transaction')->middleware('auth')->group(function () {
    Route::post('/checkout', ['as' => 'checkout', 'uses' => 'TransactionController@checkout']);
    Route::get('/', ['as' => 'transaction', 'uses' => 'TransactionController@index']);
});

Route::post('/validation/{gameId}', ['as' => 'validation', 'uses' => 'HomeController@validation']);

Route::get('/{gameId}', ['as' => 'game_details', 'uses' => 'HomeController@details']);
