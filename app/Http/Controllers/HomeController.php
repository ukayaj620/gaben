<?php

namespace App\Http\Controllers;

use App\Models\Games;
use DateTime;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('index', [
            'games' => $this->fetch_all()
        ]);
    }

    public function search(Request $request)
    {
        $query = $request->input('query');

        return view('search', [
            'games' => $this->search_result($query)
        ]);
    }

    public function details($gameId)
    {
        $game = Games::where('id', $gameId)->first();

        if ($game->adult_only == 1 && !session()->get('is_adult')) {
            return view('validation', [
                'game' => $game
            ]);
        }

        return view('details', [
            'game' => $game
        ]);
    }

    public function validation($gameId, Request $request)
    {
        $birthDate = new DateTime($request->input('birth-date'));
        $currentDate = new DateTime(date("m/d/Y"));

        $yearDiff = $currentDate->diff($birthDate)->y;

        if ($yearDiff >= 17) {
            session()->put('is_adult', true);
            $game = Games::where('id', $gameId)->first();
            return view('details', [
                'game' => $game
            ]);
        }

        return redirect('/')->with([
            'alert' => [
                'message' => 'You are still under age!',
                'color' => 'red'
            ]
        ]);
    }

    private function search_result(string $query)
    {
        $games = Games::where('name', 'like', '%' . $query . '%')->paginate(8);
        return $games;
    }

    private function fetch_all()
    {
        $games = Games::all();
        return $games;
    }
}
