<?php

namespace App\Http\Controllers;

use App\Models\Games;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function fetch_items()
    {
        $cart = session()->get('cart');
        $games = [];

        if ($cart) {
            foreach ($cart as $gameId) {
                $games[$gameId] = $this->fetch_game_details($gameId);
            }
        }

        return view('cart', [
            'games' => $games
        ]);
    }

    public function add_item(Request $request)
    {
        $gameId = $request->input('gameId');
        $cart = session()->get('cart');

        if (!$cart) {
            $cart = [$gameId];
            session()->put('cart', $cart);

            return redirect('/' . $gameId)->with([
                'alert' => [
                    'message' => 'Games successfully added to cart!',
                    'color' => 'green'
                ]
            ]);
        }

        if (in_array($gameId, $cart)) {
            return redirect('/' . $gameId)->with([
                'alert' => [
                    'message' => 'Game has already in your cart!',
                    'color' => 'red'
                ]
            ]);
        }

        array_push($cart, $gameId);
        session()->put('cart', $cart);

        return redirect('/' . $gameId)->with([
            'alert' => [
                'message' => 'Games successfully added to cart!',
                'color' => 'green'
            ]
        ]);
    }

    public function remove_item(Request $request)
    {
        if($request->id) {
            $gameId = $request->id;
    
            $cart = session()->get('cart');
            $key = array_search(strval($gameId), $cart);
    
            unset($cart[$key]);
            session()->put('cart', $cart);
    
            session()->flash('alert', [
                'message' => 'Games successfully removed from cart!',
                'color' => 'green'
            ]);
        }
    }

    private function fetch_game_details($gameId)
    {
        $game = Games::where('id', $gameId)->first();
        return $game;
    }
}
