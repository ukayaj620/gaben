<?php

namespace App\Http\Controllers;

use App\Models\Games;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Carbon\Carbon;

class TransactionController extends Controller
{
    public function index()
    {
        $cart = session()->get('cart');
        $totalPrice = 0;

        if ($cart) {
            foreach ($cart as $gameId) {
                $totalPrice += $this->fetch_game_details($gameId)->price;
            }
        } else {
            return redirect('/cart');
        }

        return view('transaction', [
            'totalPrice' => $totalPrice
        ]);
    }

    public function checkout(Request $request)
    {
        $request->validate([
            'card-name' => 'required|min:6',
            'card-number' => 'required|regex:/^(?:[0-9]{4} ){3}[0-9]{4}$/',
            'expire-date-month' => 'required|numeric|min:1|max:12',
            'expire-date-year' => 'required|numeric|min:2021|max:2050',
            'cvv' => 'required|regex:/^[0-9]{3,4}$/',
            'country' => 'required',
            'zip' => 'required|numeric',
            'total-price' => 'required'
        ]);

        $transaction = Transaction::create([
            'transaction_id' => Str::uuid()->toString(),
            'user_id' => Auth::id(),
            'purchase_date' => Carbon::now()->toDateTimeString(),
            'card_name' => $request->input('card-name'),
            'card_number' => $request->input('card-number'),
            'expire_month' => $request->input('expire-date-month'),
            'expire_year' => $request->input('expire-date-year'),
            'card_cvv' => $request->input('cvv'),
            'card_country' => $request->input('country'),
            'postal_code' => $request->input('zip'),
            'payment_total' => $request->input('total-price')
        ]);

        $cart = session()->get('cart');

        foreach ($cart as $gameId) {
            $transaction->games()->attach($this->fetch_game_details($gameId));
        }

        session()->forget('cart');

        return view('receipt', [
            'transaction' => $transaction,
        ]);
    }

    private function fetch_game_details($gameId)
    {
        $game = Games::where('id', $gameId)->first();
        return $game;
    }
}
