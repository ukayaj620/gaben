<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'username' => 'required|min:6',
            'fullname' => 'required',
            'password' => 'required|min:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x]).*$/'
        ]);

        $user = User::create([
            'name' => $request->input('fullname'),
            'username' => $request->input('username'),
            'password' => Hash::make($request->input('password'))
        ]);

        $user->role()->attach(Role::where('name', $request->input("role"))->first());

        return redirect('/login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');
        $remember = false;
        if ($request->input('remember')) {
            $remember = true;
        }

        if (Auth::attempt($credentials, $remember)) {
            $request->session()->regenerate();
            return redirect()->intended('/');
        }
  
        return redirect('/login')->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    }

    public function logout()
    {
        Auth::logout();
        session()->forget('cart');
        session()->forget('is_adult');

        return redirect('/login');
    }
}
