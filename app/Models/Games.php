<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Games extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'description',
        'long_description',
        'developer',
        'publisher',
        'price',
        'cover_path',
        'trailer_path',
        'adult_only'
    ];

    public function category()
    {
        return $this->belongsToMany(Category::class, 'category_games');
    } 
}
