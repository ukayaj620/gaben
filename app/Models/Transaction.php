<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'transaction_id',
        'user_id',
        'purchase_date',
        'card_name',
        'card_number',
        'expire_month',
        'expire_year',
        'card_cvv',
        'card_country',
        'postal_code',
        'payment_total'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function games()
    {
        return $this->belongsToMany(Games::class, 'transaction_games');
    }
}
