<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Games;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GamesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gameId = DB::table('games')->insertGetId([
            'name' => 'Apex Legend',
            'description' => 'Lorem impsum dolor amet silir. Lorem impsum dolor amet silir. Lorem impsum dolor amet silir',
            'long_description' => 'Lorem impsum dolor amet silir. Lorem impsum dolor amet silir. Lorem impsum dolor amet silir',
            'publisher' => 'Electronic Arts',
            'developer' => 'Respawn Entertament, Panic Button Games',
            'price' => '120000',
            'cover_path' => 'https://cdn.cloudflare.steamstatic.com/steam/apps/1172470/header.jpg?t=1621457566',
            'trailer_path' => 'https://cdn.cloudflare.steamstatic.com/steam/apps/256832561/movie480_vp9.webm?t=1619624593',
            'adult_only' => true
        ]);

        $game = Games::where('id', $gameId)->first();
        $game->category()->attach(Category::where('name', 'Action')->first());
        
        $gameId = DB::table('games')->insertGetId([
            'name' => 'Euro Truck Simulator 2',
            'description' => 'Lorem impsum dolor amet silir. Lorem impsum dolor amet silir. Lorem impsum dolor amet silir',
            'long_description' => 'Lorem impsum dolor amet silir. Lorem impsum dolor amet silir. Lorem impsum dolor amet silir',
            'publisher' => 'SCS Software',
            'developer' => 'SCS Software',
            'price' => '120000',
            'cover_path' => 'https://cdn.cloudflare.steamstatic.com/steam/apps/227300/header.jpg?t=1619119593',
            'trailer_path' => 'https://cdn.cloudflare.steamstatic.com/steam/apps/256827292/movie480_vp9.webm?t=1616622379',
            'adult_only' => false
        ]);

        $game = Games::where('id', $gameId)->first();
        $game->category()->attach(Category::where('name', 'Simulation')->first());

        $gameId = DB::table('games')->insertGetId([
            'name' => 'Froza Horizon 4',
            'description' => 'Lorem impsum dolor amet silir. Lorem impsum dolor amet silir. Lorem impsum dolor amet silir',
            'long_description' => 'Lorem impsum dolor amet silir. Lorem impsum dolor amet silir. Lorem impsum dolor amet silir',
            'publisher' => 'Playground Games',
            'developer' => 'Turn 10 Studios',
            'price' => '120000',
            'cover_path' => 'https://cdn.cloudflare.steamstatic.com/steam/apps/1293830/header.jpg?t=1615337540',
            'trailer_path' => 'https://cdn.cloudflare.steamstatic.com/steam/apps/256820720/movie480_vp9.webm?t=1612810706',
            'adult_only' => false
        ]);

        $game = Games::where('id', $gameId)->first();
        $game->category()->attach(Category::where('name', 'Action')->first());
    }
}
