<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->uuid('transaction_id');
            $table->date('purchase_date');
            $table->string('card_number', 255);
            $table->string('card_name', 255);
            $table->integer('expire_month');
            $table->integer('expire_year');
            $table->string('card_country', 255);
            $table->string('card_cvv', 255);
            $table->string('postal_code', 255);
            $table->unsignedBigInteger('payment_total');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
